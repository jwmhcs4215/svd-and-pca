import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.SparkContext._
import org.apache.spark.mllib.linalg.distributed.RowMatrix
import org.apache.spark.mllib.linalg.{Matrix, SingularValueDecomposition, Vector, Vectors}

val sc = new SparkContext("local", "SVD and PCA", new SparkConf())

val input = Seq(Vectors.dense(0.0, 1.0, 2.0, 1.0, 5.0, 3.3, 2.1), Vectors.dense(3.0, 4.0, 5.0, 3.1, 4.5, 5.1, 3.3), Vectors.dense(6.0, 7.0, 8.0, 2.1, 6.0, 6.7, 6.8), Vectors.dense(9.0, 0.0, 1.0, 3.4, 4.3, 1.0, 1.0))
val row_mat: RowMatrix = new RowMatrix(sc.parallelize(input, 2))

val svd: SingularValueDecomposition[RowMatrix, Matrix] = row_mat.computeSVD(7, computeU = true)
val U:RowMatrix = svd.U  
val V:Matrix = svd.V 
val D:Vector = svd.s

println("U = " + U)
println("D = " + D)
println("V = " + V)

val principal_comp:Matrix = row_mat.computePrincipalComponents(5) 

println(s"Principal Components = ${ principal_comp }")

sc.stop()
System.exit(0)